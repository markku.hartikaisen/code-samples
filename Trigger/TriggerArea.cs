using System;
using UnityEngine;

public class TriggerArea : MonoBehaviour
{
    [SerializeField] private LayerMask _layersToDetect;

    public static Action<TriggerArea, Collider, bool> OnTriggerAction;

    private void OnTriggerEnter(Collider other)
    {
        if (_layersToDetect == (_layersToDetect | (1 << other.gameObject.layer)))
        {
            OnTriggerAction?.Invoke(this, other, true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (_layersToDetect == (_layersToDetect | (1 << other.gameObject.layer)))
        {
            OnTriggerAction?.Invoke(this, other, false);
        }
    }
}