using System.Collections.Generic;
using System;

[Serializable]
public class SpeedrunSave
{
    public string Name { get; set; }
    public float Time { get; set; }
    public bool IsComputerTime { get; set; }
}

[Serializable]
public class SpeedrunLeaderboardWindow
{
    public Level.LevelID LevelID { get; set; }
    public List<SpeedrunSave> ListOfEntries { get; set; } = new List<SpeedrunSave>();
    public MedalManager.MedalType RecordMedal { get; set; } = MedalManager.MedalType.Null;
}

[Serializable]
public class PlayerData
{
    // Player information
    public string PlayerName { get; set; } = "";

    //Level information
    public int LastScene { get; set; } = 0;
    public int ScenesUnlocked { get; set; } = 0;
    public bool GameCompleted { get; set; } = false;
    public bool SpeedRunModeUnlocked { get; set; } = false;

    //Log information
    public int TerminalLog_CurrentTab { get; set; } = 0;
    public int TerminalLog_TabsAvailable { get; set; } = 0;
    public int Terminal_TabsUnlocked { get; set; } = -1;

    //Collectible information
    public List<int> CollectiblesCollected { get; } = new List<int>();

    //Speedrun information
    public List<SpeedrunLeaderboardWindow> SpeedrunLeaderboards { get; } = new List<SpeedrunLeaderboardWindow>();
}