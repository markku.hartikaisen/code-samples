using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Steamworks;
using System;

public static class SaveManager
{
    public static void SavePlayerData(PlayerData playerData)
    {
        BinaryFormatter formatter = new BinaryFormatter();

        try
        {
            using (FileStream stream = new FileStream(GetSavePath(), FileMode.Create))
            {
                formatter.Serialize(stream, playerData);
            }
        }
        catch (Exception e)
        {
            Debug.LogError($"Failed to save player data: {e.Message}");
        }
    }

    public static PlayerData LoadPlayerData()
    {
        string path = GetSavePath();
        try
        {
            if (File.Exists(path))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                using (FileStream stream = new FileStream(path, FileMode.Open))
                {
                    PlayerData data = formatter.Deserialize(stream) as PlayerData;
                    return data;
                }
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            Debug.LogError($"Failed to load player data: {e.Message}");
            return null;
        }
    }

    private static string GetSavePath()
    {
        string saveFolder = SteamManager.Initialized ? SteamUser.GetSteamID().ToString() : "user";
        string saveFile = "savefile.sav";
        string filePath = Path.Combine(Application.persistentDataPath, saveFolder);

        if (!Directory.Exists(filePath))
        {
            Directory.CreateDirectory(filePath);
        }

        return Path.Combine(filePath, saveFile);
    }
}