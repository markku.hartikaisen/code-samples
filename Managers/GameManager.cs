using System;
using Steamworks;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;
    private PlayerData _playerData = null;

    public enum GameMode { Normal, Speedrun };
    private GameMode _currentGameMode = GameMode.Normal;

    public static Action OnGameModeChange;

    public PlayerData PlayerData
    {
        get
        {
            if (_playerData == null)
            {
                _playerData = SaveManager.LoadPlayerData();

                if (_playerData == null)
                {
                    _playerData = new PlayerData();
                    SaveManager.SavePlayerData(_playerData);
                }
            }

            return _playerData;
        }
    }

    public GameMode CurrentGameMode 
    { 
        get { return _currentGameMode; }
        set
        { 
            if (_currentGameMode != value)
            {
                _currentGameMode = value;
                OnGameModeChange?.Invoke();
            }
        }   
    }

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject("GameManager");
                _instance = go.AddComponent<GameManager>();
                go.AddComponent<SteamManager>();
                DontDestroyOnLoad(go);
            }

            return _instance;
        }
    }

    private void Awake()
    {
        CurrentGameMode = GameMode.Normal;
    }

    public void GetAchievement(string achievementName)
    {
        if (SteamManager.Initialized)
        {
            SteamUserStats.GetAchievement(achievementName, out bool achievementCompleted);
            if (!achievementCompleted)
            {
                SteamUserStats.SetAchievement(achievementName);
                SteamUserStats.StoreStats();
            }
        }
    }

    public void SavePlayerData()
    {
        SaveManager.SavePlayerData(_playerData);
    }
}