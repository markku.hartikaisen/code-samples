using UnityEngine;

[CreateAssetMenu]
public class Collectible : ScriptableObject
{
    public enum CollectibleID
    {
        Collectible_0,
        Collectible_1,
        Collectible_2,
        Collectible_3,
        Collectible_4,
        Collectible_5,
        Collectible_6,
        Collectible_7,
        Collectible_8,
        Collectible_9,
        Collectible_10,
        Collectible_11,
        Collectible_12,
        Collectible_13,
        Collectible_14,
        Collectible_15,
        Collectible_16
    };

    public CollectibleID CollectibleID;

    public string Name;

    [Header("Description")]
    [TextArea(1,5)]
    public string DiscoveryText;

    [TextArea(1, 5)]
    public string Description;

    [Header("Gallery Settings")]

    public Level Level;
    public Sprite GalleryImage;
    public Sprite GallerySilhoutte;
    public Sprite GalleryBackground;
}