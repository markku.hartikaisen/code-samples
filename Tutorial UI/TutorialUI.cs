using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TutorialUI : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] private Transform _movementTutorial;
    [SerializeField] private Transform _singleButtonTutorial;
    [SerializeField] private Image _singleButtonTutorialImage;
    [SerializeField] private Transform _twoButtonTutorial;

    [Header("Sprites")]
    [SerializeField] private Sprite _jumpButtonSprite;
    [SerializeField] private Sprite _crouchButtonSprite;

    private Transform _currentTutorial = null;

    private float _appearDuration = 0.4f;
    private float _adjustDuration = 0.2f;
    private float _hideDuration = 0.3f;

    private void OnEnable()
    {
        PauseMenu.OnGamePaused += OnGamePaused;
    }

    private void OnDisable()
    {
        PauseMenu.OnGamePaused -= OnGamePaused;
    }

    private void Start()
    {
        _movementTutorial.gameObject.SetActive(false);
        _singleButtonTutorial.gameObject.SetActive(false);
        _twoButtonTutorial.gameObject.SetActive(false);
    }

    public void ShowMovementTutorial()
    {
        ShowTutorial(_movementTutorial);
    }

    public void ShowJumpTutorial()
    {
        _singleButtonTutorialImage.sprite = _jumpButtonSprite;
        ShowTutorial(_singleButtonTutorial);
    }

    public void ShowCrouchTutorial()
    {
        _singleButtonTutorialImage.sprite = _crouchButtonSprite;
        ShowTutorial(_singleButtonTutorial);
    }

    public void ShowTwoButtonTutorial()
    {
        ShowTutorial(_twoButtonTutorial);
    }

    private void ShowTutorial(Transform newTutorial)
    {
        if (GameManager.Instance.CurrentGameMode == GameManager.GameMode.Speedrun)
        {
            return;
        }

        StopAllCoroutines();
        StartCoroutine(TutorialAnimation(newTutorial));
    }

    public void HideTutorials()
    {
        StopAllCoroutines();

        if (_currentTutorial != null)
        {
            UI_SoundManager.Whoosh();
            StartCoroutine(SetSize(_currentTutorial, Vector3.zero, _hideDuration));
        }
    }

    private IEnumerator TutorialAnimation(Transform newTutorial)
    {
        if (_currentTutorial != null && _currentTutorial != newTutorial)
        {
            UI_SoundManager.Whoosh();
            yield return StartCoroutine(SetSize(_currentTutorial, Vector3.zero, 0.25f));
        }

        _currentTutorial = newTutorial;
        _currentTutorial.localScale = Vector3.zero;
        _currentTutorial.gameObject.SetActive(true);

        UI_SoundManager.Confirm();
        yield return StartCoroutine(SetSize(_currentTutorial, new Vector3(1.2f, 1.2f, 1.2f), _appearDuration));
        yield return StartCoroutine(SetSize(_currentTutorial, new Vector3(1f, 1f, 1f), _adjustDuration));
    }

    private IEnumerator SetSize(Transform trans, Vector3 targetValue, float lerpDuration)
    {
        float timeElapsed = 0;
        Vector3 startValue = trans.localScale;

        while (timeElapsed < lerpDuration)
        {
            float t = timeElapsed / lerpDuration;
            t = t * t * (3f - 2f * t);
            trans.localScale = Vector3.Lerp(startValue, targetValue, t);
            timeElapsed += Time.deltaTime;
            yield return null;
        }

        trans.localScale = targetValue;

        if (targetValue == Vector3.zero)
        {
            _currentTutorial.gameObject.SetActive(false);
            _currentTutorial = null;
        }
    }

    private void OnGamePaused(bool value)
    {
        if (_currentTutorial == null)
        {
            return;
        }

        _currentTutorial.gameObject.SetActive(!value);
    }
}